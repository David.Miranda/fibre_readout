#pragma once

#ifndef __FUCNTIONS_H_INCLUDED__
#define __FUCNTIONS_H_INCLUDED__

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdint.h>
#include <iomanip>
#include <vector>
#include <typeinfo>

#include<algorithm>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TRandom.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TAxis.h"
#include "TArrayD.h"
#include "TStyle.h"
#include "TPaveStats.h"

#include "definitions.h"
#include "DataStruct.h"

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::to_string;
using std::vector;

// Sets the same path address, name, and correct extension of the ROOT file
string TFileName(string inputN) {
  string fname = inputN;
  fname.erase(fname.find_last_of("."),string::npos);
  fname = operator+(fname,".root");
  return fname;
}

// SHIFT THE TIME CELL AND MAKES THE TIME AXIS AS A TIME LINE STARTING FROM THE TIME OF THE TRIGGER CELL
VecVec_Ft CorrectedTime (vector<tdata_t> *chtime, trigheader_t trigger) {
    VecVec_Ft temp;
    int nch = chtime->size();
    for (int i=0 ; i < nch ; i++) {
        vector<float> myVect;
        for (int k=0 ; k<1024; k++) {
            if (k==0) {
                myVect.push_back((chtime->at(i)).time[(trigger.trig_cell + k)%1024]);
            }
            else {
                myVect.push_back(myVect.at(k-1) + (chtime->at(i)).time[(trigger.trig_cell + k)%1024]);
            }
        }
        temp.push_back(myVect);
    }
    return temp;
}

VecVec_Int IntCorrectedTime (vector<tdata_t> *chtime, trigheader_t trigger) {
    VecVec_Int temp;
    int nch = chtime->size();
    for (int i=0 ; i < nch ; i++) {
        vector<int> myVect;
        for (int k=0 ; k<1024; k++) {
            myVect.push_back(k);
        }
        temp.push_back(myVect);
    }
    return temp;
}

// COMPUTE THE VOLTAGE FOR EACH CELL WITH THE RANGE DATA OF THE EVENT HEADER "eheader_t" TYPE AND NOISE SUBSTRACTION
VecVec_Ft GetVoltage(vector<edata_t> *RawWaveforms, eheaderB_t evtParameter) {
    VecVec_Ft temp;
    double range = -0.5 + evtParameter.range/1000.; /* in Volts */
    for (int i=0 ; i < RawWaveforms->size() ; i++) {
        vector<float> wave;
        for (int k=0 ; k < 1024 ; k++) {
            wave.push_back( (RawWaveforms->at(i).adc[k])/65535. + range);
        }
        temp.push_back(wave);
    }
    return temp;
}

VecVec_Int GetIntVoltage(vector<edata_t> *RawWaveforms, eheaderB_t evtParameter) {
    VecVec_Int temp;
    for (int i=0 ; i < RawWaveforms->size() ; i++) {
        vector<int> wave;
        for (int k=0 ; k < 1024 ; k++) {
            wave.push_back(RawWaveforms->at(i).adc[k]);
        }
        temp.push_back(wave);
    }
    return temp;
}

// COMPUTE THE VOLTAGE WITH NOISE SUBSTRACTION
VecVec_Ft NoiselessWaveform(VecVec_Ft *mywaveforms) {
    VecVec_Ft temp;
    for (int i=0 ; i < mywaveforms->size() ; i++) {
        float noise =0.0;
        int cnt = 0;
        vector<float> noiseless;
        for (int k=20 ; k < 171 ; k++ ) {
            noise += mywaveforms->at(i)[k];
            cnt++;
        }
        noise /= (cnt-1);
        for (int k=0 ; k < 1024 ; k++ ) {
            noiseless.push_back(mywaveforms->at(i)[k] - noise );
        }
        temp.push_back(noiseless);
    }
    return temp;
}

VecVec_Ft NoiselessIntWaveform(VecVec_Int *mywaveforms) {
    VecVec_Ft temp;
    for (int i=0 ; i < mywaveforms->size() ; i++) {
        float noise =0.0;
        int cnt = 0;
        vector<float> noiseless;
        for (int k=50 ; k < 250 ; k++ ) {
            noise += mywaveforms->at(i)[k];
            cnt++;
        }
        noise /= (cnt-1);
        for (int k=0 ; k < 1024 ; k++ ) {
            noiseless.push_back(mywaveforms->at(i)[k] - noise);
        }
        temp.push_back(noiseless);
    }
    return temp;
}

// DRAW WAVEFORMS IN CANVAS (2x2)
void DrawWaveforms(VecVec_Ft *mywaveforms, VecVec_Ft *mycorrtime, int event, const string &mystring, const std::string &my_xaxe, const std::string my_yaxe, int lcolor, int lwidth, int mcolor, int msize, int mstyle) {
//void DrawWaveforms(VecVec_Ft *mywaveforms, VecVec_Ft *mycorrtime, int event) {
    TCanvas *cSomeEvent = new TCanvas("cAnEvent","Some Event",1200,1200);
    cSomeEvent->Divide(2,2);
    int nCh = mywaveforms->size();
    TGraph *myWaveGraph;
    for (int ch=0 ; ch < nCh ; ch++) {
        myWaveGraph = new TGraph( (mywaveforms->at(ch)).size() , &mycorrtime->at(ch)[0], &mywaveforms->at(ch)[0]);
        myWaveGraph->GetXaxis()->SetTitle(my_xaxe.c_str());
        myWaveGraph->GetYaxis()->SetTitle(my_yaxe.c_str());
        myWaveGraph->SetLineWidth(lwidth);
        myWaveGraph->SetLineColor(lcolor);
        myWaveGraph->SetMarkerColor(mcolor);
        myWaveGraph->SetMarkerSize(msize);
        myWaveGraph->SetMarkerStyle(mstyle);
        myWaveGraph->SetTitle(TString::Format("Event %03d - CH%02d", event, ch));
        cSomeEvent->cd(ch+1);
        myWaveGraph->Draw("APL");
    }
    cSomeEvent->Print(mystring.c_str()+TString::Format("Event %03d.pdf",event));
}

// COMPUTING : BIN NUMBER OF MAXIMUM AMPLITUDE
vector<int> maxAmplitudeBin(VecVec_Ft *noiseless) {
    vector<float> mymaxamp;
    vector<int> mymaxampBin;
    for (int i=0 ; i < noiseless->size() ; i++) {
        float myMaxValue = 0.0;
        int myMaxValueBin = 0;
        for (int k=0 ; k < 1024 ; k++) {
            if ( myMaxValue < noiseless->at(i).at(k) ) {
                myMaxValue = noiseless->at(i).at(k);
                myMaxValueBin = k;
            }
        }
        mymaxamp.push_back(myMaxValue);
        mymaxampBin.push_back(myMaxValueBin);
    }
    return mymaxampBin;
}

// COMPUTING : CHARGE @ 1ST APPROACH
vector<float> ChargeComputation(VecVec_Ft *waveforms, VecVec_Ft *mytime, int iniWindow, int endWindow) {
    vector<float> chargeVec;
    for (int ch=0 ; ch < waveforms->size() ; ch++) {
        float mycharge = 0.0;
        for (int cell=iniWindow ; cell < endWindow ; cell++) {
            mycharge = mycharge + (mytime->at(ch).at(cell+1)-mytime->at(ch).at(cell))*waveforms->at(ch).at(cell);
        }
        chargeVec.push_back(mycharge);
    }
    return chargeVec;
}

// COMPUTING : CHARGE @ 2ND APPROACH
vector<float> ChargeFromHistoComputation( vector<TH1F> *myHistoForSpectrum, int iniWindow, int endWindow) {
    vector<float> chargeVec;
    for (int ch=0 ; ch < myHistoForSpectrum->size() ; ch++) {
        chargeVec.push_back( myHistoForSpectrum->at(ch).Integral(iniWindow,endWindow,"width"));
    }
    return chargeVec;
}

// TSPECTRUM :  HISTOGRAMS CREATION
vector<TH1F> HistoForSpectrum(VecVec_Ft *waveforms, VecVec_Ft *corrtime, int event, const string &mystring, const std::string &my_xaxe, const std::string &my_yaxe, bool printwaveform) {
    vector<TH1F> histo_vec(waveforms->size());
    for (int i=0 ; i < waveforms->size() ; i++) {
        int nPoints = corrtime->at(i).size();
        double x_low = corrtime->at(i).front();
        double x_up = corrtime->at(i).back();
        histo_vec.at(i) = TH1F(TString::Format("Histogram_evt%d_Ch%02d", event,i),TString::Format("Histogram_evt%d_Ch%02d", event,i),nPoints,x_low,x_up);
        histo_vec.at(i).GetXaxis()->SetTitle(my_xaxe.c_str());
        histo_vec.at(i).GetYaxis()->SetTitle(my_yaxe.c_str());
        histo_vec.at(i).SetDirectory(0);
        for (int k=0; k<nPoints ; k++) {
            histo_vec.at(i).SetBinContent(k+1,waveforms->at(i).at(k));
        }
    }

    if (printwaveform) {
        TCanvas *chistograms;
        chistograms = new TCanvas(TString::Format("Histograms_evt%02d",event),"Histograms for TSpectrum",1200,1200);
        chistograms->Divide(2,2);
        for (int i=0 ; i < waveforms->size() ; i++) {
            chistograms->cd(i+1);
            histo_vec.at(i).Draw("H");
            // histo_vec.at(i).Write(mystring.c_str()+TString::Format("Histogram_evt%d_Ch%02d", event,i));
        }
        chistograms->Write(mystring.c_str()+TString::Format("HistoForSpectrum_evt%02d",event));
        chistograms->Print(mystring.c_str()+TString::Format("HistoForSpectrum_evt%02d.pdf",event));
    }
    return histo_vec;
}


// COMPUTE CHARGE @ 2ND APPROX ( PEAKS FOUND WITH TSPECTRUM )
// 2ND STEP : USING TSPECTRUM TO FIND THE PEAKS AND PEAK POSITIONS
// This function uses TSpectrum to find peaks, ATTENTION: TSpectrum works with histograms and not with TGraph.
// It take volt_vec and time_vec to make a TH1 where Tspectrum Search works on give the position of the peaks
// We sort the peak position to find the first peak position
// It yields the "Bin Number" of the 1st peak of each waveform in a event
vector<double> PeaksInfo(vector<TH1F> *histo, VecVec_Ft *corrtime, int Event, int RecordedEvent) {
    vector<double> XposFirstPeak_vec;
    // TO BE SET BY THE TIME WINDOW AND TIME TRIGGER
    double time_trigger = 140.0;
//    TCanvas *cPeaksInfo;
//    if (Event == RecordedEvent) {
//        cPeaksInfo = new TCanvas("PeaksInfo%02d","Peaks Info",1200,1200);
//        cPeaksInfo->Divide(2,2);
//    }
//    if (Event == RecordedEvent) {
//        for(int i=0 ; i<histo->size() ; i++) {
//            cPeaksInfo->cd(i+1);
//            histo->at(i).Draw("H");
//            histo->at(i).Write();
//        }
//    }
    //TSpectrum peak search :
    for (int i=0 ; i<histo->size() ; i++) {
        // Setting the number of possible peaks to a big number like nPoints
        int nPoints = corrtime->at(i).size();
        TSpectrum *s = new TSpectrum(nPoints);
        // Modifiable options
        double sigmaPeak = 1.0;
        double ThresholdPeak = 0.20;
        // Searching peaks on waveforms (on the TH1F histogram)
        int nfound = s->Search(&histo->at(i),sigmaPeak,"",ThresholdPeak);
        // printf("WAVEFORM CH%d : Found %d candidate peaks\n",i,nfound);
        double *peak_pos = s->GetPositionX();
        // xPeaksPos[0] is not always the first peak. We sort the peak position to order them from first to last
        vector<double> xPeaksPos (peak_pos, peak_pos + nfound);
        // Using default comparison (operator <)
        std::sort (xPeaksPos.begin(), xPeaksPos.end());
        int npeaks = 0;
        double stored_peakpos = xPeaksPos[npeaks];
        while (xPeaksPos[npeaks] < time_trigger) {
            npeaks++;
            stored_peakpos = xPeaksPos[npeaks];
            if (npeaks == nfound-1) {
                stored_peakpos = xPeaksPos[nfound-1];
                break;
            }
        }
        XposFirstPeak_vec.push_back(stored_peakpos);
//        // Finding the Background
//        if (Event == RecordedEvent) {
//            for (int i=0 ; i<histo->size() ; i++) {
//                cPeaksInfo->cd(i+1);
//                TH1 *hb = s->Background(&histo->at(i),20,"same");
//                if (hb) {
//                    cPeaksInfo->Update();
//                }
//            }
//        }
        delete s;
    }
//    ONLY FOR DEBUGGING PURPOSE
//    if (Event == RecordedEvent) {
//        cPeaksInfo->Write(TString::Format("PeaksInfo_evt%02d",Event));
//        cPeaksInfo->Print(TString::Format("PeaksInfo_evt%02d.pdf",Event));
//    }
    return XposFirstPeak_vec;
}


// COMPUTING THE GAIN AND ITS LINEARITY
// EACH COMPOSANT OF "mygainPara" VECTOR CONTAINS AS 1ST ELEMENT THE POSITION OF THE 10TH PEAK, AS 2ND ELEMENT THE AVERAGE OF ALL THE PEAKS FOUND, AND AS THIRD TO THE END THE POSITION OF THE FIRST 10 PEAKS FOUNF WITH "TSpectrum"
VecVec_Dt GainComputation(vector<TH1F> myADC) {
    VecVec_Dt mygainPara(4);
    VecVec_Dt myPeakPos;
    TCanvas *cFingerPlots = new TCanvas("cFingerPlots","fingerplot",1200,1200);
    TCanvas *cLinearity = new TCanvas("cLinearity","Linearity",1200,1200);
    cFingerPlots->Divide(2,2);
    cLinearity->Divide(2,2);
    // TSpectrum Search : Finding the 10th peak
    for (int i=0 ; i<myADC.size() ; i++) {
        // DRAWING
        cFingerPlots->cd(i+1);
        myADC.at(i).Draw();

        int npeaks = 50;
        TSpectrum *s =  new TSpectrum(npeaks);
        // Modifiable options
        double sigmaPeak = 2.;
        double ThresholdPeak = 0.10;
        // Searching peaks on the ADC spectrum (on the TH1F histograms)
        int nfound = s->Search(&(myADC.at(i)),sigmaPeak,"",ThresholdPeak);
        printf("Uncalibrated ADC in CH0%d : Found %d candidate peaks\n",i,nfound);
        double *peak_pos = s->GetPositionX();
        // xPeaksPos[0] is not always the first peak. We sort the peak position to order them from first to last
        vector<double> xPeaksPos (peak_pos, peak_pos + nfound);
        // Using default comparison (operator <)
        std::sort (xPeaksPos.begin(), xPeaksPos.end());
        // Pushing the first 10 peaks into the array
        double Sum = 0.0;
        int cnt = 0;
        myPeakPos.push_back(xPeaksPos);
        for ( int index=0 ; index < xPeaksPos.size() && index < 10 ; index++) {
            Sum += TMath::Abs(xPeaksPos[index+1] - xPeaksPos[index]);
            cnt=index+1;
        }
        double Average = Sum / cnt;
        (mygainPara.at(i)).push_back(Average);
        delete s;
    }
    cFingerPlots->Write("FingerPlots");
    cFingerPlots->Print("FingerPlots.pdf");

    double Slope;
    double SlopeErr;
    double Ordinate;
    double OrdinateErr;
    double Chi2;

    // PLOT : LINEARITY
    TGraph *grGain[4];
    vector<int> PeakNVector;
    for (int i=0 ; i<mygainPara.size() ; i++) {
        int nPeaks = myPeakPos.at(i).size();
        int mySize;
        if (nPeaks < 10) {
            mySize = nPeaks;
        }
        else {
            mySize = 10;
        }
        vector<double> myX, myY;
        int PeakShift = 2;
        if (i==0 || i==2) {
            PeakShift = 2;
        }
        else {
            PeakShift = 0;
        }
        for (int kk=0 ; kk < mySize ; kk++) {
            myX.push_back(kk+PeakShift);
            myY.push_back(myPeakPos.at(i).at(kk));
        }

        cLinearity->cd(i+1);
        grGain[i] = new TGraph(mySize,&myX[0],&myY[0]);
        grGain[i]->SetTitle(TString::Format("Gain in CH%02d",i));
        grGain[i]->GetXaxis()->SetTitle("Peak number");
        grGain[i]->GetYaxis()->SetTitle("Peak Position [AU]");
        grGain[i]->SetMarkerColor(4);
        grGain[i]->SetMarkerStyle(20);
        grGain[i]->SetMarkerSize(0.8);
        grGain[i]->Draw("AP");

        // LINEAR FIT
        TF1 *myfunc = new TF1("myfunc","[0]*x+[1]",myX[0],myY[mySize-1]);
        myfunc->SetParNames("Slope","Ordinate");
        gStyle->SetOptFit(1111);
        grGain[i]->Fit("myfunc","S");
        cLinearity->Modified();
        cLinearity->Update();

        Slope       = myfunc->GetParameter(0);
        Ordinate    = myfunc->GetParameter(1);
        SlopeErr    = myfunc->GetParError(0);
        OrdinateErr = myfunc->GetParError(1);
        Chi2        = myfunc->GetChisquare();

        mygainPara.at(i).push_back(Slope);
        mygainPara.at(i).push_back(Ordinate);
        mygainPara.at(i).push_back(SlopeErr);
        mygainPara.at(i).push_back(OrdinateErr);
        mygainPara.at(i).push_back(Chi2);

        TPaveStats *stats =(TPaveStats*)(cLinearity->cd(i+1))->GetPrimitive("stats");
        stats->SetName("Fit Stats");
        stats->SetX1NDC(0.45);
        stats->SetX2NDC(0.85);
        stats->SetY1NDC(0.15);
        stats->SetY2NDC(0.35);
        stats->SetTextColor(4);
        gPad->Modified();

        grGain[i]->Write(TString::Format("Gain in CH%02d",i));
    }
    cLinearity->Write("Gain Linearity");
    cLinearity->Print("Gain Linearity.pdf");

    return mygainPara;
}

// SCALING ADC SPECTRA
VecVec_Dt ScalingADC(TH1F *histo, vector<double> mygainPara_atCH) {
    VecVec_Dt Data(2);
    double GainInCH = mygainPara_atCH[1] ;
    double OffsetInCH = mygainPara_atCH[2];
    int nbins = histo->GetNbinsX();
    for (int ibin=0 ; ibin<nbins; ibin++) {
        double curr_y = histo->GetBinContent(ibin+1);
        double curr_x = histo->GetBinCenter(ibin+1);
        double new_x= (OffsetInCH + curr_x)/ GainInCH;
        Data.at(0).push_back(new_x);
        Data.at(1).push_back(curr_y);
    }
    return Data;
}

// SCALING ADC SPECTRA
VecVec_Dt NewScalingADC(vector<TH1F> *histoVec, VecVec_Dt parameterVec) {
    VecVec_Dt Data(0);
    vector<TH1D> newhistoVec(0);
    VecVec_Dt NewXvect(histoVec->size());
    VecVec_Dt NewYvect(histoVec->size());
    for (int ch=0 ; ch < histoVec->size() ; ch++) {
        Printf("IN CHANNEL %d \n",ch);
        double GainInCH = parameterVec.at(ch).at(1);
        double OffsetInCH = parameterVec.at(ch).at(2);
        int nbin = histoVec->at(ch).GetNbinsX();
        printf("numbers of new bins : %d \n", nbin);
        for (int ibin=0 ; ibin < nbin ; ibin++) {
            double curr_x = histoVec->at(ch).GetBinCenter(ibin+1);
            double curr_y = histoVec->at(ch).GetBinContent(ibin+1);
            double new_x = (OffsetInCH + curr_x)/GainInCH;
            Printf("new X %f and new Y %f",new_x,curr_y);
            NewXvect.at(ch).push_back(new_x);
            NewYvect.at(ch).push_back(curr_y);
        }
    }
//    TH1D hCalibratedADC[histoVec->size()];
//    for (int ch=0 ; ch < histoVec->size() ; ch++) {
//        TH1D *newHisto = (TH1D*)histoVec->at(ch).Clone();
//        newHisto->Reset("ICESM");
//        for (int kk; kk < NewXvect.at(ch).size(); kk++) {
//            newHisto->Fill(NewXvect[kk],NewYvect[kk]);
//        }
////        TH1F hist_new=(TH1F)h->Clone();
//        int nXbins = histoVec->at(ch).GetNbinsX();
//        double lowEdge = NewXvect.at(ch).front();
//        double upEdge = NewXvect.at(ch).back();
//        hCalibratedADC[ch] = new TH1F(TString::Format("Calibrated ADC CH%02d",ch),"; ADC counts; Entries",nXbins,lowEdge,upEdge);
//        newhistoVec.push_back(*newHisto);

//    }

//    return newhistoVec;

}



// FURTHER METHODS
// RISING TIME AND TIME RESOLUTION

// Method 01 : FIX MAX AMPLITUDE DIVIDED BY "N" AS THRESHOLD
float Method01(vector<float> mytime, vector<float> mywaveform, float maxamp, float N) {
    float Inter_time = 0;
    float ThresholdLine = maxamp / N;
    float slope;
    // Scaning the waveform to intersect VoltThreshold line with waveform
    // Choosing 1/3 of the Max. Value of the waveform (for each waveform a different Maximum value)
    for (int k=10 ; k<1024 ; k++) {
        float yPoint = mywaveform.at(k);
        if ( yPoint > ThresholdLine ) {
            double Ylow = mywaveform.at(k-1);
            double Yup = mywaveform.at(k);
            double Xlow = mytime.at(k-1);
            double Xup = mytime.at(k);
            slope = (Yup-Ylow)/(Xup-Xlow);
            // Intersection X (which is time)
            Inter_time = Xlow + (ThresholdLine-Ylow)/slope;
            break;
        }
    }
    return Inter_time;

}






#endif

