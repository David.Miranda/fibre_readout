#pragma once

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include "TROOT.h"
#include "TRint.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"

#include "DataStruct.h"
#include "functions.h"

using std::vector;

typedef vector<vector<int>> VecVec_Int;
typedef vector<vector<float>> VecVec_Ft;
typedef vector<vector<double>> VecVec_Dt;

// One word buffer (4 bytes) store into a char array
// char Word[4];            // 1 word
// char FileH[4];
// char TimeH[4];
// char BoardSN[4];
// char ChannelH[4];
// char EventH[4];

TTree *tSiPM = new TTree("SiPM","SiPM");

fheader_t   fheader_Buff;     // 1 word (DRS2)
theader_t   theader_Buff;     // 1 word (TIME)
bheader_t   bheader_Buff;     // 1 word (B# ?)
chheader_t  chheader_Buff;    // 1 word (C00 ?)
tdata_t     tdata_Buff;       // 1024 words : 4 channels, each channel containts 1024 words (the type takes this into account)
eheaderA_t  eheaderA_Buff;    // 1 word
eheaderB_t  eheaderB_Buff;    // 5 words
/*bheader_t bheader_Buff;*/   // 1 word (B# ?)  commented to show duplicity in the DRS binary structure
trigheader_t trigheader_Buff; // 1 word
/*chheader_t chheader_Buff;*/ // 1 word (C00 ?) commented to show duplicity in the DRS binary structure
scaler_t    scalerheader_Buff;// 1 word for the scaler int number
edata_t     edata_Buff;       // 512 words : 1024 inputs for 1 channel, 2 bytes per Voltage Bin

// TTREE AND VARIABLES DECLARATIONS -- OK

float IntCharge[4];           TBranch *bIntCharge[4];
float IntCharge_fr01[4];      TBranch *bIntCharge_fr01[4];
float IntCharge_fr02[4];      TBranch *bIntCharge_fr02[4];
float IntCharge_fr03[4];      TBranch *bIntCharge_fr03[4];
float IntCharge_fr04[4];      TBranch *bIntCharge_fr04[4];

float Charge[4];              TBranch *bCharge[4];
float MaxAmplitude[4];        TBranch *bMaxAmplitude[4];
// float Amplitude[4][1024];    TBranch *bAmplitude[4];
float FirstPeak[4];           TBranch *bFirstPeak[4];
int   FirstPeakBin[4];        TBranch *bFirstPeakBin[4];
float Trigcell[4];            TBranch *bTrigcell[4];
float TimeMaxAmplitude[4];    TBranch *bTimeMaxAmplitude[4];
float TimePeakInfo[4];        TBranch *bTimePeakInfo[4];
float TimeWidth[4];           TBranch *bTimeWidth[4];
float Distance[4];            TBranch *bDistance[4];

float TimeONE[4];             TBranch *bTimeONE[4];
float TimeTWO[4];             TBranch *bTimeTWO[4];
float TimeTHREE[4];           TBranch *bTimeTHREE[4];
float TimeFOUR[4];            TBranch *bTimeFOUR[4];
float TimeFIVE[4];            TBranch *bTimeFIVE[4];

float TimeRes_MaxAmp;          TBranch *bTimeRes_MaxAmp;
float TimeRes_PeakInfo;        TBranch *bTimeRes_PeakInfo;
float TimeResONE[4];           TBranch *bTimeResONE[4];
float TimeResTWO[4];           TBranch *bTimeResTWO[4];
float TimeResTHREE[4];         TBranch *bTimeResTHREE[4];
float TimeResFOUR[4];          TBranch *bTimeResFOUR[4];
float TimeResFIVE[4];          TBranch *bTimeResFIVE[4];

// HISTROGRAMS DECLARATION -- OK
TH2F *hWaveforms[4];
TH2F *hNoiseless[4];
TH2F *hIntWaveforms[4];
TH2F *hIntNoiseless[4];
TH2F *hCoAmplitude[4];

TH2F *hCorr[4];

TH1F *hCharge[4];
TH1D *hADC[4];
TH1F *hIntCharge[4];
TH1D *hIntADC[4];
TH1F *hMaxAmplitude[4];
TH1F *hFirstPeakBin[4];
TH1F *hFirstPeak[4];
TH1F *hTimeRes_PeakInfo;
TH1F *hTimeWidth[4];

TH1F *hIntCharge_fr01[4];
TH1D *hIntADC_fr01[4];
TH1F *hIntCharge_fr02[4];
TH1D *hIntADC_fr02[4];
TH1F *hIntCharge_fr03[4];
TH1D *hIntADC_fr03[4];
TH1F *hIntCharge_fr04[4];
TH1D *hIntADC_fr04[4];

TH1F *hTimeAmplitude[4];
TH1F *hTimePeakInfo[4];
TH1F *hTimeONE[4];
TH1F *hTimeTWO[4];
TH1F *hTimeTHREE[4];
TH1F *hTimeFOUR[4];
TH1F *hTimeFIVE[4];

TH1F *hTimeRes_MaxAmp;

TH1F *hTimeRes_MethONE;
TH1F *hTimeRes_MethTWO;
TH1F *hTimeRes_MethTHREE;
TH1F *hTimeRes_MethFOUR;
TH1F *hTimeRes_MethFIVE;

// HISTOGRAMS SETTING VALUES
int nCells    = 1024; //-- OK
int nCounts   = 500; // --OK
int nEntries  = 500; // -- OK
// for waveforms -- OK
int VoltBins = TMath::Power(2,16);
int TimeBins = 1024;
float intTimeWindow = 1024;
// for waveforms -- OK
double tmin   = 0.;
double tmax   = 430.;
// if RC at 0 (-0.15 V to 0.95 V) -- OK
int nVolts = 1024;
double Vlow   = -0.1;
double Vup    = 1.10;
// for fingerplots -- OK
double cntmin = -2.0;
double cntmax = 40.0;
// for cells -- OK
int minBin    = 0;
int maxBin    = 1023;

int DiscardEvent=0;

//Time intervals in channels
TGraphErrors *TimeInterval[4];

// The values of tmin or tmax depend of the time calibration (different values for 1GSPS and 5GSPS)
// For 5.12 GSPS

int bins = 1024;

int nPhE = 70;



#endif // DEFINITIONS_H
