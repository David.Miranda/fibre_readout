#pragma once

#include <iostream>
#include <cstdint>
#include <vector>
#include <iomanip>


struct fheader_t{
    char h1 : 8;    // "D"
    char h2 : 8;    // "R"
    char h3 : 8;    // "S"
    char h4 : 8;    // "2"
};

struct theader_t{
    char h1 : 8;    // "T"
    char h2 : 8;    // "I"
    char h3 : 8;    // "M"
    char h4 : 8;    // "E"
};

struct bheader_t{
    char h1             :  8;    // "B"
    char h2             :  8;    // "#"
    std::uint16_t b_num : 16;
};

struct chheader_t{
    char h1     : 8;    // "C"
    char h2     : 8;    // "0"
    char h3     : 8;    // "0"
    char ch_id  : 8;
};

struct tdata_t{
    float time[1024];
};
// struct tdata_t{
//     tdata_t() : time(1024) {}; // Declaration of a constructor for a fixed vector size initialisation
//     std::vector<std::uint32_t> time;
// };

struct eheaderA_t{
    char h1 : 8;    // "E"
    char h2 : 8;    // "H"
    char h3 : 8;    // "D"
    char h4 : 8;    // "R"
};

struct eheaderB_t{

    std::uint32_t ev_cnt      : 32;
    std::uint16_t year        : 16;
    std::uint16_t month       : 16;
    std::uint16_t day         : 16;
    std::uint16_t hour        : 16;
    std::uint16_t minute      : 16;
    std::uint16_t second      : 16;
    std::uint16_t millisecond : 16;
    std::uint16_t range       : 16;
};

struct trigheader_t{
    char h1     : 8;  // "T"
    char h2     : 8;  // "#"
    std::uint16_t trig_cell;
};

struct scaler_t{
    std::uint32_t scaler;
};

struct edata_t{
    std::uint16_t adc[1024];
};

std::ostream& operator<<(std::ostream& os, fheader_t& obj) {
    return os << obj.h1 << obj.h2 << obj.h3 << obj.h4;
}

std::ostream& operator<<(std::ostream& os, theader_t& obj) {
    return os << obj.h1 << obj.h2 << obj.h3 << obj.h4;
}

std::ostream& operator<<(std::ostream& os, bheader_t& obj) {
    return os << obj.h1 << obj.h2 << " " << obj.b_num;
}

std::ostream& operator<<(std::ostream& os, chheader_t& obj) {
    return os << obj.h1 << obj.h2 <<  obj.h3 << " " << obj.ch_id << '\n';
}

std::ostream& operator<<(std::ostream& os, tdata_t& obj) {
    for (unsigned i=0; i<15; ++i){
        os << i << " : " << std::setprecision(15) << obj.time[i] << '\n';
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, eheaderA_t& obj) {
    os << obj.h1 << obj.h2 << obj.h3 << obj.h4 << "\n";
    return os;
}
std::ostream& operator<<(std::ostream& os, eheaderB_t& obj) {
    os << "Year: "      << obj.year         << '\n'
       << "Month: "     << obj.month        << '\n'
       << "Day: "       << obj.day          << '\n'
       << "Hour: "      << obj.hour         << '\n'
       << "Minute: "    << obj.minute       << '\n'
       << "Second: "    << obj.second       << '\n'
       << "Millisec: "  << obj.millisecond  << '\n'
       << "Range: "     << obj.range        << '\n';
    return os;
}

std::ostream& operator<<(std::ostream& os, trigheader_t& obj) {
    os << obj.h1 << obj.h2 << " " << obj.trig_cell << '\n';
    return os;
}

std::ostream& operator<<(std::ostream& os, scaler_t& obj) {
    os << "Scaler: " << obj.scaler << '\n';
    return os;
}

std::ostream& operator<<(std::ostream& os, edata_t& obj) {
    for (unsigned i=0; i<5; ++i){
        os << i << " : " << obj.adc[i] << '\n';
    }
    return os;
}
