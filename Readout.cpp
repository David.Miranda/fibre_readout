// Program that reads a binary file created by a PSI DRS evaluation board version 5.0
// This program only reads a binary file produced by 1 board only
// It is made to read up to 4 independent channels
// call :
// ./Readout <binary file> <n_event> <n_boards> <n_active_ch_per_board> <Distance>
// LUIS DAVID MEDINA MIRANDA

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdint.h>
#include <iomanip>
#include <vector>
#include <typeinfo>
#include <time.h>

#include<algorithm>

#include "TROOT.h"
#include "TRint.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TPad.h"
#include "TString.h"
#include "TPaveLabel.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TSpectrum.h"
#include "TAxis.h"
#include "TArrayD.h"

#include "DataStruct.h"
#include "functions.h"
#include "definitions.h"

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::to_string;
using std::vector;


int main(int argc, char *argv[]) {

  /* STARTING CLOCK */
  clock_t tStart = clock();

  if(argc<5) {
    cout << "usage: drs_bin2root <binary file> <n_event> <n_boards> <n_active_ch_per_board> <distance [mm]>" << endl;
    return -1;
  }

  // // // // // // // // // // //
  //  INPUT FILE AND BUFFERING  //
  // // // // // // // // // // //

  /* STRING FOR PATH : declaration of string called ifname and initializing with the argv[1] */
  std::string ifname(argv[1]);
  /* OPENING THE BINARY FILE */
  std::ifstream ifs(ifname, std::ios::binary);

  const unsigned long nEvents = atoi(argv[2]);
  const unsigned long nBoards = atoi(argv[3]);
  const signed int nCh = atoi(argv[4]);
  const float distanceFrom1SiPM = atof(argv[5]);
  float distanceFrom3SiPM = 600 - distanceFrom1SiPM;

  /* BINARY FILE LENGTH */
  ifs.seekg (0, std::ios::end);
  unsigned int length = ifs.tellg();
  ifs.seekg (0, std::ios::beg);


  if(!ifs.is_open()) {
    cout << "file not opened" << '\n';
  }
  else {
    cout << "length of the file in characters  " << length << '\n';
  }

  /* ROOT FILE : Creation and Initialization */
  TFile *MyROOTfile = new TFile (TFileName(ifname).c_str(),"RECREATE");


  // // // // // // // // // // //
  //    CREATING ROOT OBJECTS   //
  // // // // // // // // // // //

  /* Histograms */
  for (Int_t i=0 ; i< nCh ; i++) {

      /* ----- TH2 ----- */
      /* Waveform display */
      hWaveforms[i]    = new TH2F(TString::Format("Waveforms CH%02d",i),"; Time [ns]; Amplitude [V]",nCells,tmin,tmax,nCells,Vlow,Vup);
      hNoiseless[i]    = new TH2F(TString::Format("Noiseless CH%02d",i),"; Time [ns]; Amplitude [V]",nCells,tmin,tmax,nCells,Vlow,Vup);

      hIntWaveforms[i] = new TH2F(TString::Format("Integer Waveforms CH%02d",i),"; Time [Bin]; Amplitude [Bin]",TimeBins,0,TimeBins-1,VoltBins,0,VoltBins-1);
      hIntNoiseless[i] = new TH2F(TString::Format("Integer Noiseless CH%02d",i),"; Time [Bin]; Amplitude [Bin]",TimeBins,0,TimeBins-1,VoltBins,(0.-4000.),((VoltBins-1)-4000.));

      /* ----- TH1 ----- */
      /* Integer ADC without calibration */
      hIntCharge[i]         = new TH1F(TString::Format("Integer Charge CH%02d",i),"; ADC counts; Entries",VoltBins*intTimeWindow, (0-2*TMath::Power(10,6)), ((VoltBins-1)*intTimeWindow)-2*TMath::Power(10,6));
      /* Integer ADC with calibration */
      hIntADC[i]            = new TH1D(TString::Format("Integer ADC CH%02d",i),"; ADC counts; Entries",VoltBins, 0, VoltBins-1);

      hIntCharge_fr01[i]         = new TH1F(TString::Format("Integer Charge CH%02d fr01",i),"; ADC counts; Entries",VoltBins*intTimeWindow, (0-2*TMath::Power(10,6)), ((VoltBins-1)*intTimeWindow)-2*TMath::Power(10,6));
      hIntCharge_fr02[i]         = new TH1F(TString::Format("Integer Charge CH%02d fr02",i),"; ADC counts; Entries",VoltBins*intTimeWindow, (0-2*TMath::Power(10,6)), ((VoltBins-1)*intTimeWindow)-2*TMath::Power(10,6));
      hIntCharge_fr03[i]         = new TH1F(TString::Format("Integer Charge CH%02d fr03",i),"; ADC counts; Entries",VoltBins*intTimeWindow, (0-2*TMath::Power(10,6)), ((VoltBins-1)*intTimeWindow)-2*TMath::Power(10,6));
      hIntCharge_fr04[i]         = new TH1F(TString::Format("Integer Charge CH%02d fr04",i),"; ADC counts; Entries",VoltBins*intTimeWindow, (0-2*TMath::Power(10,6)), ((VoltBins-1)*intTimeWindow)-2*TMath::Power(10,6));
      hIntADC_fr01[i]            = new TH1D(TString::Format("Integer ADC CH%02d fr01",i),"; ADC counts; Entries",VoltBins, 0, VoltBins-1);
      hIntADC_fr02[i]            = new TH1D(TString::Format("Integer ADC CH%02d fr02",i),"; ADC counts; Entries",VoltBins, 0, VoltBins-1);
      hIntADC_fr03[i]            = new TH1D(TString::Format("Integer ADC CH%02d fr03",i),"; ADC counts; Entries",VoltBins, 0, VoltBins-1);
      hIntADC_fr04[i]            = new TH1D(TString::Format("Integer ADC CH%02d fr04",i),"; ADC counts; Entries",VoltBins, 0, VoltBins-1);



      /* ADC without calibration */
      hCharge[i]            = new TH1F(TString::Format("ChargeCH%02d",i),"; ADC counts; Entries",TMath::Power(2,10), -2.00, 50.00);
      /* ADC with calibration */
      hADC[i]               = new TH1D(TString::Format("ADC_CH%02d",i),"; ADC counts; Entries",500, -2.00, 50.00);
      /* Distribution of Time cell width */
      hTimeWidth[i]         = new TH1F(TString::Format("CellWidthCH%02d",i),"; Width [ns]; Entries",nCells/4,Vlow,Vup);
      /* Distribution of Max. Amplitude */
      hMaxAmplitude[i]      = new TH1F(TString::Format("MaxAmplitudeCH%02d",i),"; Amplitude [V]; Entries",1024, -0.10, 1.00);
      /* Distribution of the Time of the Max. Amplitude (Rising Time) */
      hTimeAmplitude[i]     = new TH1F(TString::Format("Time of Max. Amplitude in CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);

      /* After TSpectrum : Distribution of Bin index of the 1st Peak */
      hFirstPeakBin[i]      = new TH1F(TString::Format("FirstPeakBinCH%02d",i),"; Bin number; Entries",nCells,minBin,maxBin);
      /* After TSpectrum : Distribution of the Amplitudes of the 1st Peak */
      hFirstPeak[i]         = new TH1F(TString::Format("FirstPeakCH%02d",i),"; Amplitude [V]; Entries",nVolts,Vlow,Vup);
      /* After TSpectrum : Distribution of the Time of the 1st Peak occurrence (Rising Time) */
      hTimePeakInfo[i]      = new TH1F(TString::Format("Time of 1st Peak in CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);

      /* Distribution of the rising time from different methods */
      hTimeONE[i]      = new TH1F(TString::Format("Rising Time Method 1 - CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);
      hTimeTWO[i]      = new TH1F(TString::Format("Rising Time Method 2 - CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);
      hTimeTHREE[i]    = new TH1F(TString::Format("Rising Time Method 3 - CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);
      hTimeFOUR[i]     = new TH1F(TString::Format("Rising Time Method 4 - CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);
      hTimeFIVE[i]     = new TH1F(TString::Format("Rising Time Method 5 - CH%02d",i),"; Time [ns]; Entries",1024,tmin,tmax);
  }

  /* TIME RESOLUTION */
  /* distribution of TIME RESOLUTION from Time of Max. Amplitude */
  hTimeRes_MaxAmp    = new TH1F("Time of Max. Amplitude","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  /* distribution of TIME RESOLUTION from Time of 1st Peak occurrence */
  hTimeRes_PeakInfo  = new TH1F("Time of 1st Peak","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  /* distribution of TIME RESOLUTION (t_sig02 - t_sig01) from different methods */
  hTimeRes_MethONE   = new TH1F("Time resolution of Method 1","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  hTimeRes_MethTWO   = new TH1F("Time resolution of Method 2","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  hTimeRes_MethTHREE = new TH1F("Time resolution of Method 3","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  hTimeRes_MethFOUR  = new TH1F("Time resolution of Method 4","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);
  hTimeRes_MethFIVE  = new TH1F("Time resolution of Method 5","; t_sig02 - t_sig01 [ns]; Entries",100,-20,20);


  /* Branches */
  for (Int_t i=0 ; i< nCh ; i++) {
      /* BRANCHES for TTree */
      bCharge[i]            = tSiPM->Branch(TString::Format("ChargeCH%02d",i),&Charge[i],TString::Format("ChargeCH%02d/F",i));

      bIntCharge[i]         = tSiPM->Branch(TString::Format("IntegerChargeCH%02d",i),&IntCharge[i],TString::Format("IntegerChargeCH%02d/F",i));
      bIntCharge_fr01[i]    = tSiPM->Branch(TString::Format("IntegerChargefr01CH%02d",i),&IntCharge_fr01[i],TString::Format("IntegerChargefr01CH%02d/F",i));
      bIntCharge_fr02[i]    = tSiPM->Branch(TString::Format("IntegerChargefr02CH%02d",i),&IntCharge_fr02[i],TString::Format("IntegerChargefr02CH%02d/F",i));
      bIntCharge_fr03[i]    = tSiPM->Branch(TString::Format("IntegerChargefr03CH%02d",i),&IntCharge_fr03[i],TString::Format("IntegerChargefr03CH%02d/F",i));
      bIntCharge_fr04[i]    = tSiPM->Branch(TString::Format("IntegerChargefr04CH%02d",i),&IntCharge_fr04[i],TString::Format("IntegerChargefr04CH%02d/F",i));

      // bAmplitude[i]         = tSiPM->Branch(TString::Format("Amplitude%02d",i),&Amplitude[i],TString::Format("AmplitudeCH%02d[1024]/F",i));
      bMaxAmplitude[i]      = tSiPM->Branch(TString::Format("MaxAmplitude%02d",i),&MaxAmplitude[i],TString::Format("MaxAmplitudeCH%02d/F",i));
      bFirstPeak[i]         = tSiPM->Branch(TString::Format("FirstPeakCH%02d",i),&FirstPeak[i],TString::Format("FirstPeakCH%02d/F",i));
      bTrigcell[i]          = tSiPM->Branch(TString::Format("trigCellCH%02d",i),&Trigcell[i],TString::Format("TrigCellCH%02d/F",i));
      bTimeMaxAmplitude[i]  = tSiPM->Branch(TString::Format("Time from Max. Amplitude CH%02d",i),&TimeMaxAmplitude[i],TString::Format("Time Max. Amplitude CH%02d/F",i));
      bTimePeakInfo[i]      = tSiPM->Branch(TString::Format("Time from PeakInfo CH%02d",i),&TimePeakInfo[i],TString::Format("Time from PeakInfo CH%02d/F",i));
      bTimeWidth[i]         = tSiPM->Branch(TString::Format("CellWidthCH%02d",i),&TimeWidth[i],TString::Format("CellWidthCH%02d/F",i));
      bDistance[i]          = tSiPM->Branch(TString::Format("DistanceCH%02d",i),&Distance[i],TString::Format("DistanceCH%02d/F",i));
  }

  bTimeRes_MaxAmp    = tSiPM->Branch("Timing from Max. Amplitude",&TimeRes_MaxAmp,"TimingFromMaxAmp/F");
  bTimeRes_PeakInfo  = tSiPM->Branch("Timing from 1st Peak",&TimeRes_PeakInfo,"TimingFromPeakInfo/F");

  // // // // // // // // // // // // //
  // READING THE BINARY DATA FOR TIME //
  // // // // // // // // // // // // //

  /* Reading FILE HEADER */
  ifs.read((char*)&fheader_Buff, sizeof(fheader_Buff));
  cout << *(&fheader_Buff) << endl;
  /* Reading TIME HEADER */
  ifs.read((char*)&theader_Buff, sizeof(theader_Buff));
  cout << *(&theader_Buff) << endl;
  /* Reading BOARD HEADER */
  ifs.read((char*)&bheader_Buff, sizeof(bheader_Buff));
  cout << *(&bheader_Buff) << endl;
  /* LOOP over channels to get TIME DATA and further STORAGE in the "ChTime" variable, i = CHANNEL NUMBER */
  vector<tdata_t> ChTime;
  for (Int_t i=0 ; i< nCh ; i++) {
      /* Reading CHANNEL HEADER */
      ifs.read((char*)&chheader_Buff,sizeof(chheader_Buff));
      /* Reading TIME DATA */
      ifs.read((char*)&tdata_Buff,sizeof(tdata_Buff));
      // cout << *(&tdata_Buff) << endl;
      ChTime.push_back(*((tdata_t*)&tdata_Buff));
  }

  /* FILLING HISTOGRAM : Disbribution of cell width for the time channels */
  VecVec_Ft TimeWidth(0);
  for (int i=0 ; i < nCh ; i++) {
      vector<float> temp;
      for (int k=0 ; k < nCells ; k++) {
          hTimeWidth[i]->Fill(ChTime.at(i).time[k]);
          temp.push_back(ChTime.at(i).time[k]);
      }
      TimeWidth.push_back(temp);
  }

  // // // // // // // // // // // // // // // // // // // // // // // // //
  //  FROM NOW ON, WE WORK WITH "TimeWidth" AND NOT ANYMORE WITH "ChTime" //
  // // // // // // // // // // // // // // // // // // // // // // // // //


  // // // // // // // // // // // // // //
  // READING THE BINARY DATA FOR EVENTS  //
  // // // // // // // // // // // // // //

  /* LOOP over the "events" */
  for (int evt=0 ; evt < nEvents ; evt++) {
      if (evt%10000==0) {
          printf("EVENT : %d\n", evt);
      }

      /* Reading 1nd part of EVENT HEADER */
      ifs.read((char*)&eheaderA_Buff,sizeof(eheaderA_Buff));
      /* Reading 2nd part of EVENT HEADER */
      ifs.read((char*)&eheaderB_Buff,sizeof(eheaderB_Buff));
      eheaderB_t eventH = *((eheaderB_t*)&eheaderB_Buff);
      // cout << "Event Header " << eventH << endl;
      /* Reading the BOARD HEADER - important for multiboard configuration */
      ifs.read((char*)&bheader_Buff,sizeof(bheader_Buff));
      // Reading the TRIGGER CELL - each event and each channel has a different trigger cell)
      ifs.read((char*)&trigheader_Buff,sizeof(trigheader_Buff));
      trigheader_t trigger = *((trigheader_t*)&trigheader_Buff);
      // cout << "Trigger Cell " << trigger << endl;

      /* INTEGER VOLTAGE WAVEFORM : its size is defined by the number of channel */
      /* LOOP over channels to get EVENT DATA and further STORAGE in the "RawWaveforms" variable, i = CHANNEL NUMBER */
      vector<edata_t> RawWaveforms(0);
      /* LOOP over "channels" for each events */
      for (int i=0 ; i<nCh ; i++) {
          /* Reading the CHANNEL HEADER */
          ifs.read((char*)&chheader_Buff,sizeof(chheader_Buff));
          /* Reading the SCALER */
          ifs.read((char*)&scalerheader_Buff,sizeof(scalerheader_Buff));
          /* Reading INTEGER EVENT DATA */
          ifs.read((char*)&edata_Buff,sizeof(edata_Buff));
          RawWaveforms.push_back(*((edata_t*)&edata_Buff));
      }
      /* AT THE END OF THIS LOOP, I GOT A 4-DIM VECTOR OF "edata_t" TYPE, CALLED "RawWaveforms" */
      /* "RawWaveforms" CONTAINS A 4 "edata_t" VECTORS WHICH ARE THE WAVEFORMS FOR EACH CHANNEL */
      /* THIS TYPE HAS AN ARRAY WHICH NEEDS TO BE ACCESS TO COMPUTE DIFFERENT OBSERVABLES USING OTHER TYPES AS THE "trigheader_t" */


      /* PRIMITIVE VECTORS : "vector<tdata_t> ChTime" and "vector<edata_t> RawWaveforms" */
      /* MAIN VECTORS      : "VecVec_Ft myCorrTime"   and "VecVec_Ft myNoiseless" */
      VecVec_Int myIntCorrTime  = IntCorrectedTime(&ChTime,trigger);
      VecVec_Int myIntVolt      = GetIntVoltage(&RawWaveforms,eventH);
      VecVec_Ft  myIntNoiseless = NoiselessIntWaveform(&myIntVolt);

      VecVec_Ft myCorrTime = CorrectedTime(&ChTime,trigger);
      VecVec_Ft myWaveform = GetVoltage(&RawWaveforms,eventH);
      VecVec_Ft myNoiseless= NoiselessWaveform(&myWaveform);


      // // // // // // // // //
      //   DISCARDING EVENT   //
      // // // // // // // // //

      bool DISCARD = false;
      bool DISCARD_SPIKY = false;
      bool DISCARD_NOISY = false;

      for (int ch=0; ch < nCh ; ch++) {
          float max = *std::max_element(myNoiseless.at(ch).begin(), myNoiseless.at(ch).end());
          if ( max< 0.005 ) {
              DISCARD_NOISY = true;
              // Printf("NOISY %d : CH%d -> MAX %f :\n", evt, ch, max);
          }
          if ( max > 0.9 ) {
              DISCARD_SPIKY = true;
              // Printf("SPIKY %d : CH%d -> MAX %f :\n", evt, ch, max);
          }
      }

      if (DISCARD_NOISY==true || DISCARD_SPIKY==true) {
          DISCARD = true;
          DiscardEvent++;
      }

      // // // // // // // // //
      //   DISCARDING EVENT   //
      // // // // // // // // //

      // // // // // // // // //
      //   PRINT A WAVEFORM   //
      // // // // // // // // //


      /* Integer Waveform */
      /* Print in this interval of events */
      if (evt>4859 && evt<4871) {
          VecVec_Ft IntTime(0);
          VecVec_Ft IntVolt(0);
          for (int ch=0 ; ch<nCh ; ch++) {
              vector<float> tempTime((myIntCorrTime.at(ch)).begin(), (myIntCorrTime.at(ch)).end());
              vector<float> tempVolt((myIntNoiseless.at(ch)).begin(), (myIntNoiseless.at(ch)).end());
              IntTime.push_back(tempTime);
              IntVolt.push_back(tempVolt);
          }
          int LColor = 8; int LWidth = 2;
          int MColor = 6; int MSize = 1; int MStyle = 43;
          Printf("Printing Integer Waveform %d \n",evt);
          DrawWaveforms( &IntVolt,  &IntTime, evt, "Integer_", "Time [Bin]", "Amplitude [Bin]",LColor,LWidth,MColor,MSize,MStyle);
      }
      /* Float Waveform */
      /* Print in this interval of events */
      if (evt<0) {
          int LColor = 9; int LWidth = 2;
          int MColor = 6; int MSize = 1; int MStyle = 43;
          Printf("Printing Float Waveform %d \n",evt);
          DrawWaveforms( &myNoiseless,  &myCorrTime, evt, "Float_", "Time [ns]", "Amplitude [V]",LColor,LWidth,MColor,MSize,MStyle);
      }

      if (DISCARD == true && evt<0) {
          Printf("Bad event %d \n",evt);
          if (DISCARD_SPIKY==true) {
              int LColor = 4; int LWidth = 2;
              int MColor = 2; int MSize = 1; int MStyle = 43;
              DrawWaveforms( &myNoiseless,  &myCorrTime, evt, "Spiky_Bad_", "Time [ns]", "Amplitude [V]",LColor,LWidth,MColor,MSize,MStyle);
          }
          if (DISCARD_NOISY==true) {
              int LColor = 2; int LWidth = 2;
              int MColor = 4; int MSize = 1; int MStyle = 41;
              DrawWaveforms( &myNoiseless,  &myCorrTime, evt, "Noisy_Bad_", "Time [ns]", "Amplitude [V]",LColor,LWidth,MColor,MSize,MStyle);
          }
      }

      // // // // // // // // //
      //   PRINT A WAVEFORM   //
      // // // // // // // // //


      // // // // // // // // // // // //
      // WORKING WITH GOOD WAVEFORMS : //
      // // // // // // // // // // // //

      if (!DISCARD) {

          /* TSPECTRUM NEEDS HISTOGRAMS : return a vector of histograms, each element contains the waveform of channel[element] */
          bool PrintHisto = false;
          /* Print in this interval of events */
          if (evt<0) {
              PrintHisto = true;
          }

          VecVec_Ft IntTime(0);
          VecVec_Ft IntVolt(0);
          for (int ch=0 ; ch<nCh ; ch++) {
              vector<float> tempTime((myIntCorrTime.at(ch)).begin(), (myIntCorrTime.at(ch)).end());
              vector<float> tempVolt((myIntNoiseless.at(ch)).begin(), (myIntNoiseless.at(ch)).end());
              IntTime.push_back(tempTime);
              IntVolt.push_back(tempVolt);
          }

          /* TSPECTRUM NEEDS HISTOGRAMS : Create a set of Histogram */
          vector<TH1F> myHistoForSpectrum     = HistoForSpectrum(&myNoiseless,&myCorrTime,evt,"Float_", "Time [ns]", "Amplitude [V]", PrintHisto);
          vector<TH1F> myIntHistoForSpectrum  = HistoForSpectrum(&IntVolt,&IntTime,evt,"Integ_", "Time [Bin]", "Amplitude [Bin]", PrintHisto);

          // TO DO :

          float lowBound = 350;
          float upBound  = 600;
          vector<float> PreCalibrationADC = ChargeComputation(&IntVolt, &IntTime,lowBound,upBound);
          vector<float> PreCalibrationADCfromHisto = ChargeFromHistoComputation(&myIntHistoForSpectrum,lowBound,upBound);

          float lowBound_fr01 = 350;
          float upBound_fr01  = 1000;

          float lowBound_fr02 = 350;
          float upBound_fr02  = 800;

          float lowBound_fr03 = 350;
          float upBound_fr03  = 500;

          float lowBound_fr04 = 300;
          float upBound_fr04  = 450;

          vector<float> PreCalibrationADC_fr01          = ChargeComputation(&IntVolt, &IntTime,lowBound_fr01,upBound_fr01);
          vector<float> PreCalibrationADCfromHisto_fr01 = ChargeFromHistoComputation(&myIntHistoForSpectrum,lowBound_fr01,upBound_fr01);
          vector<float> PreCalibrationADC_fr02          = ChargeComputation(&IntVolt, &IntTime,lowBound_fr02,upBound_fr02);
          vector<float> PreCalibrationADCfromHisto_fr02 = ChargeFromHistoComputation(&myIntHistoForSpectrum,lowBound_fr02,upBound_fr02);
          vector<float> PreCalibrationADC_fr03          = ChargeComputation(&IntVolt, &IntTime,lowBound_fr03,upBound_fr03);
          vector<float> PreCalibrationADCfromHisto_fr03 = ChargeFromHistoComputation(&myIntHistoForSpectrum,lowBound_fr03,upBound_fr03);
          vector<float> PreCalibrationADC_fr04          = ChargeComputation(&IntVolt, &IntTime,lowBound_fr04,upBound_fr04);
          vector<float> PreCalibrationADCfromHisto_fr04 = ChargeFromHistoComputation(&myIntHistoForSpectrum,lowBound_fr04,upBound_fr04);




         // ADC FOR INTEGER waveforms / FLOAT WAVEFORMS -> FIX BIN WIDTH / VARIABLE BIN WIDTH (ALSO IMPLEMENT DYNAMIC WINDOW OF INTEGRATION) + GAIN AND CALIBRATION
         // HISTOS : SetBinContent Fix BIN WIDTH / Variable BIN WIDTH
         // MAKE DIFFERENCES of HISTO, plots difference of some waveforms (2 waveforms overimposed) and distribution
          // MAKE CORRELATION PLOTS)


          /* Computing the INTEGRAL CHARGE (float and integer) */
          vector<float> ChargeVector    = ChargeComputation(&myNoiseless,&myCorrTime,200,720);
          vector<float> IntChargeVector = ChargeComputation(&myIntNoiseless,&myCorrTime,350,600); // REDUNDANTE???? CHECK

          /* Computing the THE BIN INDEX CORRESPONDING TO THE MAX. AMPLITUDE OF A WAVEFORM : used to compute the MAX. AMPLITUDE and the TIME of the MAX. AMPLITUDE ocurrence */
          vector<int> indexMaxAmpVector = maxAmplitudeBin(&myNoiseless);
          vector<float> MaxAmpVector(0);
          vector<float> MaxAmpTimeVector(0);
          for (int i =0 ; i<nCh ; i++) {
              /* Computing the MAX. AMPLITUDE */
              MaxAmpVector.push_back(myNoiseless.at(i).at(indexMaxAmpVector[i]));

          // // // // // // // // // //
          // ----- Rising Time ----- //
          // // // // // // // // // //

              /* Computing the TIME (of Rising Time) OF MAX. AMPLITUDE OCURRENCE */
              MaxAmpTimeVector.push_back(myCorrTime.at(i).at(indexMaxAmpVector[i]));
          }
          /* Computing the Time (or Rising Time) of the 1st Peak with TSPECTRUM (TSPECTRUM works with doubles) */
          vector<double> myFirstPeakTimeVector = PeaksInfo(&myHistoForSpectrum,&myCorrTime,evt,evt);

          vector<float> RisingTime_Meth01(0); /* */
          for (int i=0 ; i<nCh ; i++) {
              RisingTime_Meth01.push_back( Method01(myCorrTime.at(i),myNoiseless.at(i),MaxAmpVector.at(i),3) );
          }



          // // // // // // // // // // // // // // // // // //
          // ----- Time Resolution (t_sig02 - t_sig01) ----- //
          // // // // // // // // // // // // // // // // // //

          float timeRes_MaxAmp   = MaxAmpTimeVector[3]- MaxAmpTimeVector[1];
          float timeRes_PeakInfo = myFirstPeakTimeVector[3] - myFirstPeakTimeVector[1];
          float timeRes_Meth01 = RisingTime_Meth01[3] - RisingTime_Meth01[1]; /* */


          // // // // // // // // //
          //  FILLING HISTOGRAMS  //
          // // // // // // // // //

          /* TH2 : filling waveforms on top of each other for each channel */
          for (int i=0 ; i< nCh ; i++) {
              for (int k=0 ; k< nCells ; k++) {
                  hWaveforms[i]->Fill( myCorrTime[i].at(k) , myWaveform[i].at(k) );
                  hNoiseless[i]->Fill( myCorrTime[i].at(k) , myNoiseless[i].at(k) );
                  hIntWaveforms[i]->Fill( myIntCorrTime[i].at(k) , myIntVolt[i].at(k) );
                  hIntNoiseless[i]->Fill( myIntCorrTime[i].at(k) , myIntNoiseless[i].at(k) );
              }
          }


          for (int i=0 ; i < nCh ; i++) {
              // MAX AMPLITUDES DISTRIBUTION AND 1ST TIMING APPROXIMATION FROM AMPLITUDE MAX. VALUE
              hMaxAmplitude[i]->Fill(MaxAmpVector[i]);
              hTimeAmplitude[i]->Fill(MaxAmpTimeVector[i]);
              // INTEGRAL CHARGE
              hCharge[i]->Fill(ChargeVector[i]);
              hIntCharge[i]->Fill(IntChargeVector[i]); // REDUNDANTE???? CHECK
//              hIntCharge[i]->Fill(PreCalibrationADCfromHisto[i]);
              hIntCharge_fr01[i]->Fill(PreCalibrationADCfromHisto_fr01[i]);
              hIntCharge_fr02[i]->Fill(PreCalibrationADCfromHisto_fr02[i]);
              hIntCharge_fr03[i]->Fill(PreCalibrationADCfromHisto_fr03[i]);
              hIntCharge_fr04[i]->Fill(PreCalibrationADCfromHisto_fr04[i]);
              // MAX AMPLITUDE FROM 1ST PEAK FOUND WITH TSPECTRUM
              hTimePeakInfo[i]->Fill(myFirstPeakTimeVector[i]);

              /* TH1 : Rising Time method */
              hTimeONE[i]->Fill(RisingTime_Meth01[i]); /* */
              // hTimeTWO[i]->Fill(RisingTime[i]);
              // hTimeTHREE[i]->Fill(RisingTime[i]);
              // hTimeFOUR[i]->Fill(RisingTime[i]);
              // hTimeFIVE[i]->Fill(RisingTime[i]);
          }

          hTimeRes_MaxAmp->Fill(TimeRes_MaxAmp);
          hTimeRes_PeakInfo->Fill(TimeRes_PeakInfo);
          hTimeRes_MethONE->Fill(timeRes_Meth01); /* */

          // // // // // // // // //
          //   FILLING BRANCHES   //
          // // // // // // // // //

          for (int i=0 ; i < nCh ; i++) {
              Charge[i]          = ChargeVector[i];
              IntCharge[i]       = PreCalibrationADC[i];
              IntCharge_fr01[i]  = PreCalibrationADC_fr01[i];
              IntCharge_fr02[i]  = PreCalibrationADC_fr02[i];
              IntCharge_fr03[i]  = PreCalibrationADC_fr03[i];
              IntCharge_fr04[i]  = PreCalibrationADC_fr04[i];
              MaxAmplitude[i]       = MaxAmpVector[i];
              TimePeakInfo[i]  = myFirstPeakTimeVector[i];
              TimeMaxAmplitude[i] = MaxAmpTimeVector[i];
              if (i==1) {
                  Distance[i] = distanceFrom1SiPM;
              }
              else if (i==3) {
                  Distance[i] = distanceFrom3SiPM;
              }
              else {
                  Distance[i] = 0.00;
              }
              // TIMING METHODS : BRANCH
              TimeRes_MaxAmp = timeRes_MaxAmp;
              TimeRes_PeakInfo = timeRes_PeakInfo;

              tSiPM->Fill();
          }

      } // END "IF NOT DISCARD"
  } // END OF "EVENTS LOOP"

  for (int i=0 ;  i<nCh ; i++) {
      hIntCharge[i]->Rebin(2048);
      hIntCharge_fr01[i]->Rebin(2048);
      hIntCharge_fr02[i]->Rebin(2048);
      hIntCharge_fr03[i]->Rebin(2048);
      hIntCharge_fr04[i]->Rebin(2048);
//      hIntCharge[i]->Write(TString::Format("myRebinedADC%0d",i));
  }

  Printf("DISCARDED events %d :\n",DiscardEvent);


  // RESIZING THE HISTO FOR ADC SPECTRA //
  // RESIZING THE HISTO FOR ADC SPECTRA //
  // RESIZING THE HISTO FOR ADC SPECTRA //

  TH1F *hResizedADC[4];
  TCanvas *cUncalibratedADC = new TCanvas("cUncalibratedADC","Uncalibrated ADC",1200,1200);
  cUncalibratedADC->Divide(2,2);
  vector<TH1F> UnCalibratedADC(0);
  for (int i=0 ;  i<nCh ; i++) {
      //Create a new Histogram and fill it from bin 512 to 2560 =  2048 bins, and use this to get the Gain
      int lowerlimitBin = 512;
      int uplimitBin = 2560;
      int NumBins = uplimitBin - lowerlimitBin;
      double lowedge = hIntCharge[i]->GetXaxis()->GetBinLowEdge(lowerlimitBin);
      double upedge = hIntCharge[i]->GetXaxis()->GetBinLowEdge(uplimitBin);
      hResizedADC[i] = new TH1F(TString::Format("UnCalibrated_ADC_CH%02d",i),TString::Format("UnCalibrated_ADC_CH%02d",i),NumBins,lowedge,upedge);
      int nBins = hResizedADC[i]->GetNbinsX();
      for (int ibin = 0 ; ibin < nBins ; ibin++) {
          double content = hIntCharge[i]->GetBinContent(lowerlimitBin + ibin);
          hResizedADC[i]->SetBinContent(ibin, content);
      }
      UnCalibratedADC.push_back(*hResizedADC[i]);
      cUncalibratedADC->cd(i+1);
      hResizedADC[i]->Draw("hist");
      hResizedADC[i]->Write();
  }
  cUncalibratedADC->Write("ADC.pdf");
  cUncalibratedADC->Print("ADC.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TH1F *hResizedADC_fr01[4];
  TCanvas *cUncalibratedADC_fr01 = new TCanvas("cUncalibratedADC_fr01","Uncalibrated ADC",1200,1200);
  cUncalibratedADC_fr01->Divide(2,2);
  vector<TH1F> UnCalibratedADC_fr01(0);
  for (int i=0 ;  i<nCh ; i++) {
      //Create a new Histogram and fill it from bin 512 to 2560 =  2048 bins, and use this to get the Gain
      int lowerlimitBin = 512;
      int uplimitBin = 2560;
      int NumBins = uplimitBin - lowerlimitBin;
      double lowedge = hIntCharge_fr01[i]->GetXaxis()->GetBinLowEdge(lowerlimitBin);
      double upedge = hIntCharge_fr01[i]->GetXaxis()->GetBinLowEdge(uplimitBin);
      hResizedADC_fr01[i] = new TH1F(TString::Format("UnCalibrated_ADC_fr01_CH%02d",i),TString::Format("UnCalibrated_ADC_fr01_CH%02d",i),NumBins,lowedge,upedge);
      int nBins = hResizedADC_fr01[i]->GetNbinsX();
      for (int ibin = 0 ; ibin < nBins ; ibin++) {
          double content = hIntCharge_fr01[i]->GetBinContent(lowerlimitBin + ibin);
          hResizedADC_fr01[i]->SetBinContent(ibin, content);
      }
      UnCalibratedADC_fr01.push_back(*hResizedADC[i]);
      cUncalibratedADC_fr01->cd(i+1);
      hResizedADC_fr01[i]->Draw("hist");
      hResizedADC_fr01[i]->Write();
  }
  cUncalibratedADC_fr01->Write("ADC_fr01.pdf");
  cUncalibratedADC_fr01->Print("ADC_fr01.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TH1F *hResizedADC_fr02[4];
  TCanvas *cUncalibratedADC_fr02 = new TCanvas("cUncalibratedADC_fr02","Uncalibrated ADC",1200,1200);
  cUncalibratedADC_fr02->Divide(2,2);
  vector<TH1F> UnCalibratedADC_fr02(0);
  for (int i=0 ;  i<nCh ; i++) {
      //Create a new Histogram and fill it from bin 512 to 2560 =  2048 bins, and use this to get the Gain
      int lowerlimitBin = 512;
      int uplimitBin = 2560;
      int NumBins = uplimitBin - lowerlimitBin;
      double lowedge = hIntCharge_fr02[i]->GetXaxis()->GetBinLowEdge(lowerlimitBin);
      double upedge = hIntCharge_fr02[i]->GetXaxis()->GetBinLowEdge(uplimitBin);
      hResizedADC_fr02[i] = new TH1F(TString::Format("UnCalibrated_ADC_fr02_CH%02d",i),TString::Format("UnCalibrated_ADC_fr02_CH%02d",i),NumBins,lowedge,upedge);
      int nBins = hResizedADC_fr02[i]->GetNbinsX();
      for (int ibin = 0 ; ibin < nBins ; ibin++) {
          double content = hIntCharge_fr02[i]->GetBinContent(lowerlimitBin + ibin);
          hResizedADC_fr02[i]->SetBinContent(ibin, content);
      }
      UnCalibratedADC_fr02.push_back(*hResizedADC[i]);
      cUncalibratedADC_fr02->cd(i+1);
      hResizedADC_fr02[i]->Draw("hist");
      hResizedADC_fr02[i]->Write();
  }
  cUncalibratedADC_fr02->Write("ADC_fr02.pdf");
  cUncalibratedADC_fr02->Print("ADC_fr02.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TH1F *hResizedADC_fr03[4];
  TCanvas *cUncalibratedADC_fr03 = new TCanvas("cUncalibratedADC_fr03","Uncalibrated ADC",1200,1200);
  cUncalibratedADC_fr03->Divide(2,2);
  vector<TH1F> UnCalibratedADC_fr03(0);
  for (int i=0 ;  i<nCh ; i++) {
      //Create a new Histogram and fill it from bin 512 to 2560 =  2048 bins, and use this to get the Gain
      int lowerlimitBin = 512;
      int uplimitBin = 2560;
      int NumBins = uplimitBin - lowerlimitBin;
      double lowedge = hIntCharge_fr03[i]->GetXaxis()->GetBinLowEdge(lowerlimitBin);
      double upedge = hIntCharge_fr03[i]->GetXaxis()->GetBinLowEdge(uplimitBin);
      hResizedADC_fr03[i] = new TH1F(TString::Format("UnCalibrated_ADC_fr03_CH%02d",i),TString::Format("UnCalibrated_ADC_fr03_CH%02d",i),NumBins,lowedge,upedge);
      int nBins = hResizedADC_fr03[i]->GetNbinsX();
      for (int ibin = 0 ; ibin < nBins ; ibin++) {
          double content = hIntCharge_fr03[i]->GetBinContent(lowerlimitBin + ibin);
          hResizedADC_fr03[i]->SetBinContent(ibin, content);
      }
      UnCalibratedADC_fr03.push_back(*hResizedADC[i]);
      cUncalibratedADC_fr03->cd(i+1);
      hResizedADC_fr03[i]->Draw("hist");
      hResizedADC_fr03[i]->Write();
  }
  cUncalibratedADC_fr03->Write("ADC_fr03.pdf");
  cUncalibratedADC_fr03->Print("ADC_fr03.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TH1F *hResizedADC_fr04[4];
  TCanvas *cUncalibratedADC_fr04 = new TCanvas("cUncalibratedADC_fr04","Uncalibrated ADC",1200,1200);
  cUncalibratedADC_fr04->Divide(2,2);
  vector<TH1F> UnCalibratedADC_fr04(0);
  for (int i=0 ;  i<nCh ; i++) {
      //Create a new Histogram and fill it from bin 512 to 2560 =  2048 bins, and use this to get the Gain
      int lowerlimitBin = 512;
      int uplimitBin = 2560;
      int NumBins = uplimitBin - lowerlimitBin;
      double lowedge = hIntCharge_fr04[i]->GetXaxis()->GetBinLowEdge(lowerlimitBin);
      double upedge = hIntCharge_fr04[i]->GetXaxis()->GetBinLowEdge(uplimitBin);
      hResizedADC_fr04[i] = new TH1F(TString::Format("UnCalibrated_ADC_fr04_CH%02d",i),TString::Format("UnCalibrated_ADC_fr04_CH%02d",i),NumBins,lowedge,upedge);
      int nBins = hResizedADC_fr04[i]->GetNbinsX();
      for (int ibin = 0 ; ibin < nBins ; ibin++) {
          double content = hIntCharge_fr04[i]->GetBinContent(lowerlimitBin + ibin);
          hResizedADC_fr04[i]->SetBinContent(ibin, content);
      }
      UnCalibratedADC_fr04.push_back(*hResizedADC[i]);
      cUncalibratedADC_fr04->cd(i+1);
      hResizedADC_fr04[i]->Draw("hist");
      hResizedADC_fr04[i]->Write();
  }
  cUncalibratedADC_fr04->Write("ADC_fr04.pdf");
  cUncalibratedADC_fr04->Print("ADC_fr04.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/


  /* Calibrating with the Gain and offset */
  VecVec_Dt myGainParameters      = GainComputation(UnCalibratedADC);
  VecVec_Dt myGainParameters_fr01 = GainComputation(UnCalibratedADC_fr01);
  VecVec_Dt myGainParameters_fr02 = GainComputation(UnCalibratedADC_fr02);
  VecVec_Dt myGainParameters_fr03 = GainComputation(UnCalibratedADC_fr03);
  VecVec_Dt myGainParameters_fr04 = GainComputation(UnCalibratedADC_fr04);

//  NewScalingADC(&UnCalibratedADC, myGainParameters);

  // SCALING THE HISTO FOR ADC SPECTRA //
  // SCALING THE HISTO FOR ADC SPECTRA //
  // SCALING THE HISTO FOR ADC SPECTRA //

  TCanvas *cCalibratedADC = new TCanvas("cCalibratedADC","Calibrated ADC",1200,1200);
  cCalibratedADC->Divide(2,2);
  TH1F *hCalibratedADC[4];
  for (int i=0 ; i<nCh ; i++) {
      int NumBins = UnCalibratedADC.at(i).GetNbinsX();
      hCalibratedADC[i] = new TH1F(TString::Format("Calibrated_ADC_CH%02d",i),TString::Format("Calibrated_ADC_CH%02d",i),NumBins,-2.0,50.0);
      VecVec_Dt myData = ScalingADC(hResizedADC[i],myGainParameters.at(i));
      vector<double> myX = myData.at(0);
      vector<double> myY = myData.at(1);
      for (Int_t kk=0 ; kk<myX.size() ; kk++) {
          hCalibratedADC[i]->Fill(myX[kk],myY[kk]);
      }
      cCalibratedADC->cd(i+1);
      hCalibratedADC[i]->Draw("hist");
      hCalibratedADC[i]->Write();
  }

  cCalibratedADC->Write("CalibratedADC.pdf");
  cCalibratedADC->Print("CalibratedADC.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TCanvas *cCalibratedADC_fr01 = new TCanvas("cCalibratedADC_fr01","Calibrated ADC",1200,1200);
  cCalibratedADC_fr01->Divide(2,2);
  TH1F *hCalibratedADC_fr01[4];
  for (int i=0 ; i<nCh ; i++) {
      int NumBins = UnCalibratedADC_fr01.at(i).GetNbinsX();
      hCalibratedADC_fr01[i] = new TH1F(TString::Format("Calibrated_ADC_fr01_CH%02d",i),TString::Format("Calibrated_ADC_fr01_CH%02d",i),NumBins,-2.0,50.0);
      VecVec_Dt myData = ScalingADC(hResizedADC_fr01[i],myGainParameters_fr01.at(i));
      vector<double> myX = myData.at(0);
      vector<double> myY = myData.at(1);
      for (Int_t kk=0 ; kk<myX.size() ; kk++) {
          hCalibratedADC_fr01[i]->Fill(myX[kk],myY[kk]);
      }
      cCalibratedADC_fr01->cd(i+1);
      hCalibratedADC_fr01[i]->Draw("hist");
      hCalibratedADC_fr01[i]->Write();
  }

  cCalibratedADC->Write("CalibratedADC_fr01.pdf");
  cCalibratedADC->Print("CalibratedADC_fr01.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TCanvas *cCalibratedADC_fr02 = new TCanvas("cCalibratedADC_fr02","Calibrated ADC",1200,1200);
  cCalibratedADC_fr02->Divide(2,2);
  TH1F *hCalibratedADC_fr02[4];
  for (int i=0 ; i<nCh ; i++) {
      int NumBins = UnCalibratedADC_fr02.at(i).GetNbinsX();
      hCalibratedADC_fr02[i] = new TH1F(TString::Format("Calibrated_ADC_fr02_CH%02d",i),TString::Format("Calibrated_ADC_fr02_CH%02d",i),NumBins,-2.0,50.0);
      VecVec_Dt myData = ScalingADC(hResizedADC_fr02[i],myGainParameters_fr02.at(i));
      vector<double> myX = myData.at(0);
      vector<double> myY = myData.at(1);
      for (Int_t kk=0 ; kk<myX.size() ; kk++) {
          hCalibratedADC_fr02[i]->Fill(myX[kk],myY[kk]);
      }
      cCalibratedADC_fr02->cd(i+1);
      hCalibratedADC_fr02[i]->Draw("hist");
      hCalibratedADC_fr02[i]->Write();
  }

  cCalibratedADC->Write("CalibratedADC_fr02.pdf");
  cCalibratedADC->Print("CalibratedADC_fr02.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TCanvas *cCalibratedADC_fr03 = new TCanvas("cCalibratedADC_fr03","Calibrated ADC",1200,1200);
  cCalibratedADC_fr03->Divide(2,2);
  TH1F *hCalibratedADC_fr03[4];
  for (int i=0 ; i<nCh ; i++) {
      int NumBins = UnCalibratedADC_fr03.at(i).GetNbinsX();
      hCalibratedADC_fr03[i] = new TH1F(TString::Format("Calibrated_ADC_fr03_CH%02d",i),TString::Format("Calibrated_ADC_fr03_CH%02d",i),NumBins,-2.0,50.0);
      VecVec_Dt myData = ScalingADC(hResizedADC_fr03[i],myGainParameters_fr03.at(i));
      vector<double> myX = myData.at(0);
      vector<double> myY = myData.at(1);
      for (Int_t kk=0 ; kk<myX.size() ; kk++) {
          hCalibratedADC_fr03[i]->Fill(myX[kk],myY[kk]);
      }
      cCalibratedADC_fr03->cd(i+1);
      hCalibratedADC_fr03[i]->Draw("hist");
      hCalibratedADC_fr03[i]->Write();
  }

  cCalibratedADC->Write("CalibratedADC_fr03.pdf");
  cCalibratedADC->Print("CalibratedADC_fr03.pdf");

  /*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*//*/*/

  TCanvas *cCalibratedADC_fr04 = new TCanvas("cCalibratedADC_fr04","Calibrated ADC",1200,1200);
  cCalibratedADC_fr04->Divide(2,2);
  TH1F *hCalibratedADC_fr04[4];
  for (int i=0 ; i<nCh ; i++) {
      int NumBins = UnCalibratedADC_fr04.at(i).GetNbinsX();
      hCalibratedADC_fr04[i] = new TH1F(TString::Format("Calibrated_ADC_fr04_CH%02d",i),TString::Format("Calibrated_ADC_fr04_CH%02d",i),NumBins,-2.0,50.0);
      VecVec_Dt myData = ScalingADC(hResizedADC_fr04[i],myGainParameters_fr04.at(i));
      vector<double> myX = myData.at(0);
      vector<double> myY = myData.at(1);
      for (Int_t kk=0 ; kk<myX.size() ; kk++) {
          hCalibratedADC_fr04[i]->Fill(myX[kk],myY[kk]);
      }
      cCalibratedADC_fr04->cd(i+1);
      hCalibratedADC_fr04[i]->Draw("hist");
      hCalibratedADC_fr04[i]->Write();
  }

  cCalibratedADC->Write("CalibratedADC_fr04.pdf");
  cCalibratedADC->Print("CalibratedADC_fr04.pdf");

  for (Int_t i=0 ; i< nCh ; i++) {
      int ampCells = hMaxAmplitude[i]->GetXaxis()->GetNbins();
      float minAmp = hMaxAmplitude[i]->GetXaxis()->GetBinLowEdge(1);
      float maxAmp = hMaxAmplitude[i]->GetXaxis()->GetBinUpEdge(1024);
      printf("Amplitude : Cells %d , min %f , max %f \n",ampCells,minAmp,maxAmp);

      int chCells  = hCalibratedADC[i]->GetXaxis()->GetNbins();
      float minChar = hCalibratedADC[i]->GetXaxis()->GetBinLowEdge(1);
      float maxChar = hCalibratedADC[i]->GetXaxis()->GetBinUpEdge(2048);
      printf("Charge : Cells %d , min %f , max %f \n",chCells,minChar,maxChar);

      hCorr[i]    = new TH2F(TString::Format("Correlation CH%02d",i),"; Amplitude [V]; Charge [V]",ampCells,minAmp,maxAmp,chCells,minChar,maxChar);

  }

//  for (Int_t i=0 ; i< nCh ; i++) {
//      for (int ibin = 1 ; ibin < 1024 ; ibin++) {
//          float xContent = hMaxAmplitude[i]->GetBinContent(ibin);
//      }
//      for (int ibin = 1 ; ibin < 2048 ; ibin++) {
//          float yContent = hCalibratedADC[i]->GetBinContent(ibin);
//      }

//  }


  for (int i=0 ; i < nCh ; i++) {
      for (int ibin = 1 ; ibin < hCalibratedADC[i]->GetXaxis()->GetNbins() ; ibin++) {
          IntCharge[i]       = hCalibratedADC[i]->GetBinContent(ibin) ;
          IntCharge_fr01[i]  = hCalibratedADC_fr01[i]->GetBinContent(ibin);
          IntCharge_fr02[i]  = hCalibratedADC_fr02[i]->GetBinContent(ibin);
          IntCharge_fr03[i]  = hCalibratedADC_fr03[i]->GetBinContent(ibin);
          IntCharge_fr04[i]  = hCalibratedADC_fr04[i]->GetBinContent(ibin);
          tSiPM->Fill();
      }
  }


  /* Saving Histrograms and TTree on the ROOT file */

  for (int i=0 ; i<nCh ; i++) {
      hWaveforms[i]->Write();
      hNoiseless[i]->Write();
      hIntWaveforms[i]->Write();
      hIntNoiseless[i]->Write();

      hMaxAmplitude[i]->Write();
      hTimeAmplitude[i]->Write();
      hCharge[i]->Write();

      hIntCharge[i]->Write();
      hIntCharge_fr01[i]->Write();
      hIntCharge_fr02[i]->Write();
      hIntCharge_fr03[i]->Write();
      hIntCharge_fr04[i]->Write();

      hADC[i]->Write(TString::Format("ADC_CH%02d",i));

      hTimePeakInfo[i]->Write();
      hTimeAmplitude[i]->Write();
      hTimeWidth[i]->Write();
      // hCoAmplitude[i]->Write();
      hTimeONE[i]->Write();
      hTimeTWO[i]->Write();
      hTimeTHREE[i]->Write();
      hTimeFOUR[i]->Write();
      hTimeFIVE[i]->Write();
  }

  hTimeRes_MaxAmp->Write();
  hTimeRes_PeakInfo->Write();
  hTimeRes_MethONE->Write();
  hTimeRes_MethTWO->Write();
  hTimeRes_MethTHREE->Write();
  hTimeRes_MethFOUR->Write();
  hTimeRes_MethFIVE->Write();

  tSiPM->Write();

  TCanvas *cCorr = new TCanvas("Corr","Corr",1200,1200);
  cCorr->Divide(2,2);
  for (Int_t i=0 ; i<nCh ; i++) {
      TH2F *corrTH2F = new TH2F(Form("CorrTH2F%02d",i),"; Charge ; Amplitude ",2048,-5.,30.0,1024,-0.1,1.0);
      cCorr->cd(i+1);
      tSiPM->Draw(Form("MaxAmplitudeCH%02d:ChargeCH%02d>>CorrTH2F%02d",i,i,i),"","COLZ");
      corrTH2F->Write(Form("CorrTH2F%02d",i));
  }

//  TCanvas *cCorr_fr01 = new TCanvas("Corr_fr01","Corr_fr01",1200,1200);
//  cCorr_fr01->Divide(2,2);
//  for (Int_t i=0 ; i<nCh ; i++) {
//      TH2F *corrTH2F_fr01 = new TH2F(Form("CorrTH2F_fr01%02d",i),"; Charge ; Amplitude ",2048,-5.,30.0,1024,-0.1,1.0);
//      cCorr_fr01->cd(i+1);
//      tSiPM->Draw(Form("MaxAmplitudeCH%02d:IntegerChargeCH_fr01%02d>>CorrTH2F_fr01%02d",i,i,i),"","COLZ");
//      corrTH2F_fr01->Write(Form("CorrTH2F_fr01%02d",i));
//  }


  /* Closing the ROOT file */
  MyROOTfile->Close();

  printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
  return 0;

} // End main



